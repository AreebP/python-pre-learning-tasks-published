def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        answer = a + b
        print(answer)
    elif operator == "-":
        answer = a - b
        print(answer)
    elif operator == "/":
        answer = a / b
        print(answer)
    elif operator == "*":
        answer = a * b
        print(answer)

    # ==============


calculator(2, 4, "+")   # Should print 6 to the console
calculator(10, 3, "-")  # Should print 7 to the console
calculator(4, 7, "*")  # Should print 28 to the console
calculator(100, 2, "/")  # Should print 50 to the console
