def vowel_swapper(string):
    for x in string:
        if x == "a":
            string = string.replace("a", "4")

        elif x == "A":
            string = string.replace("A", "4")

        elif x == "e":
            string = string.replace("e", "3")

        elif x == "E":
            string = string.replace("E", "3")

        elif x == "i":
            string = string.replace("i", "!")

        elif x == "I":
            string = string.replace("I", "!")

        elif x == "o":
            string = string.replace("o", "ooo")

        elif x == "O":
            string = string.replace("O", "000")

        elif x == "u":
            string = string.replace("u", "|_|")

        elif x == "U":
            string = string.replace("U", "|_|")

    print(string)


vowel_swapper("aA eE iI oO uU")  # Should print "44 33 !! ooo000 |_||_|" to the console
vowel_swapper("Hello World")  # Should print "H3llooo Wooorld" to the console
vowel_swapper("Everything's Available")  # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
